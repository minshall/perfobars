module.exports = {
    root: true,
    env: {
        browser: true,
        es2021: true,
    },
    extends: [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
        "plugin:prettier/recommended",
    ],
    parserOptions: {
        ecmaVersion: 12,
        sourceType: "module",
        tsconfigRootDir: __dirname,
        project: ["./tsconfig.json"],
    },
    rules: {
        eqeqeq: "error",
        "no-template-curly-in-string": "error",
        quotes: ["error", "double"],
        semi: [2, "always"],
        "@typescript-eslint/prefer-namespace-keyword": "off",
        "prefer-const": "off",
        "prettier/prettier": "warn",
    },
    plugins: ["@typescript-eslint", "prettier"],
};
