// perfobars.ts

/**
 * @module Perfobars
 */

import * as d3 from "d3";
import * as _ from "lodash";

// https://babeljs.io/docs/en/babel-polyfill
// we leave this for the caller, if they feel the need
// XXX DOCUMENT ME!!!
// import "core-js";

// this is a large'ish module, so the user may not want it to load
// most of the time.  so, import "dynamically".  if it works, great.
// if it doesn't work, the user "didn't want it" (or, didn't know,
// sigh), so no problem.
let saveSvgAsPng:
    | undefined
    | typeof import("types-save-svg-as-png").saveSvgAsPng = undefined;
import("save-svg-as-png")
    .then(
        (module: typeof import("types-save-svg-as-png")) =>
            (saveSvgAsPng = module.saveSvgAsPng)
    )
    .catch(() => (saveSvgAsPng = undefined)); // not even a message

// performance measures:
// https://developer.mozilla.org/en-US/docs/Web/API/Performance/measure
// to see all:     performance.getEntriesByType("measure")
// window.performance.getEntries().filter(d => /^data.js.*listener/.test(d.name))

// track open timings
let outstanding = new Map<string, number>();

const engFormatNumber = (d: number) =>
    Intl.NumberFormat(undefined, {
        notation: "engineering",
    })
        .format(d)
        .replace(/E0$/, "")
        .replace(/E/, "e");
const formatNumber = (d: number) =>
    d > 1 ? Intl.NumberFormat().format(d) : engFormatNumber(d);

/**
 * Options - options that can be passed in to `new Perfobars`
 *
 * @param html - an instance of `HtmlOptions`
 *
 * @param svg - an instance of `SvgOptions`
 */
export type Options = {
    html: HtmlOptions; // the html options
    svg: SvgOptions; // the svg options
};

/**
 * HtmlOptions describe where and how Perfobars is inserted in the
 * page.
 *
 * @param place - a `jquery` selector locating a place in the page for
 *      Perfobar's user interface elements
 *
 * @param displayClass - class for the `div` created to hold the UI
 * elements
 *
 * @param buttonsClass - class for the `div` created to hold the buttons
 *
 * @param buttonsAboveDisplay - if `true`, the buttons are above, as
 * opposed to below, the display
 *
 * @param buttons - an instance of `ButtonOptions`
 */
export type HtmlOptions = {
    place: string; // jquery selector
    displayClass: string; // what class to assign to the display
    buttonsClass: string; //what class the buttons are assigned
    buttonsAboveDisplay: boolean; // buttons go above or below display
    buttons: ButtonOptions[]; //describes the available buttons
};

/**
 * ButtonOptions allows the developer to configure how the
 * buttons Perfobars adds are inserted into the DOM.
 * @param addClasses - more classes added to this button
 * @param name - which button is being configured
 * @param classes - some of the classes we add to this button
 * @param text - text to appear on the button [`name`]
 */
export type ButtonOptions = {
    name: "generate" | "clear" | "remove" | "save"; // which button
    classes: string[];
    addClasses: string[];
    text: string;
};

/**
 * SvgOptions - details of the SVG holding the display
 *
 * @param aspectRatio - aspect ratio (height/width) [0.6]
 * @param imageName - name of saved .png ["perfobars-image.png"]
 * @param imageBackgroundColor - background color of saved .png ["grey"]
 * @param minimumWidth - minimum width on screen [300]
 */
export type SvgOptions = {
    aspectRatio: number;
    imageName: string;
    imageBackgroundColor: string;
    minimumWidth: number;
};

/**
 * these are the defaults, 'as shipped', for Perfobars.  many
 * hard-coded parameters, both html and svg (say), could easily be
 * moved into here.  they can be retrieved with the `defaultOptions`
 * method.
 */
const installedDefaultOptions: Options = {
    html: {
        place: "#perfobars-display",
        displayClass: "perfobars-display",
        buttonsClass: "perfobars-buttons",
        buttonsAboveDisplay: true,
        buttons: [
            {
                name: "generate",
                classes: ["generate"],
                addClasses: ["btn", "btn-light"],
                text: "generate chart",
            },
            {
                name: "clear",
                classes: ["clear"],
                addClasses: ["btn", "btn-light"],
                text: "clear counters",
            },
            {
                name: "remove",
                classes: ["remove onlychart"],
                addClasses: ["btn", "btn-light"],
                text: "remove chart",
            },
            {
                name: "save",
                classes: ["save onlychart"],
                addClasses: ["btn", "btn-light"],
                text: "save chart",
            },
        ],
    },
    svg: {
        aspectRatio: 0.6, // height/width
        minimumWidth: 300, // for when the screen is initially very shrunk
        imageName: "perfobars-image.png", // for save
        imageBackgroundColor: "grey", // background color of saved .png
    },
};
// a new instance inherits these.  these can be modified by
// Perfobars.defaultOptions
let defaultOptions = installedDefaultOptions;

export default class Perf {
    clientName: string;
    options: Options;
    place: d3.Selection<HTMLDivElement, unknown, HTMLElement, unknown>;
    displayDivName: string;
    displayDivClass: string;
    buttonsDivName: string;
    buttonsDivClass: string;
    buttonsSelect: d3.Selection<
        HTMLDivElement,
        ButtonOptions,
        HTMLElement,
        unknown
    >;
    displaySelect: d3.Selection<
        d3.BaseType,
        SVGSVGElement,
        HTMLElement,
        unknown
    >;
    width: number;
    height: number;
    rects: d3.Selection<d3.BaseType, PerformanceEntry, SVGGElement, undefined>;
    xAxis: (
        g: d3.Selection<SVGGElement, undefined, null, undefined>
    ) => d3.Selection<SVGGElement, undefined, null, undefined>;
    yAxis: (
        g: d3.Selection<SVGGElement, undefined, null, undefined>
    ) => d3.Selection<SVGGElement, undefined, null, undefined>;
    svg: d3.Selection<SVGSVGElement, undefined, null, undefined>;
    ttdiv: d3.Selection<SVGGElement, undefined, null, undefined>;
    x: d3.ScaleLinear<number, number, never>;
    y: d3.ScaleBand<string>;
    measures: PerformanceEntry[];
    gx: d3.Selection<SVGSVGElement, undefined, null, undefined>;
    gy: d3.Selection<SVGSVGElement, undefined, null, undefined>;
    /**
     * CLIENTNAME is an arbitrary name for the user of *this* class
     * instance.  all classes share the same graphic, etc.
     *
     * by default, Perfobars attaches its elements after a DOM node with
     * 'id="perfobars-display"'.  so, the *default* value of ARGPLACE is
     * "#perfobars-display" (NB: notice the "#").  but, if *any* instance
     * is constructed with a non-null/undefined ARGPLACE, that is
     * where the graph (for all instances) will show up
     *
     * three buttons are instantiated:
     * - generate :: this generates a graph, using whatever data is in the database
     * - clear :: clears the database
     * - remove :: removes the graph from the DOM
     *
     * @param clientName - the name ("tag") of this client
     * @param options - options to override defaults
     */
    constructor(
        clientName: string,
        options: {
            [key in keyof Options]: typeof defaultOptions[key];
        } = installedDefaultOptions
    ) {
        this.clientName = clientName;
        this.options = options
            ? mergeOptions(defaultOptions, options)
            : defaultOptions;
        this.place = d3.select<HTMLDivElement, unknown>(
            this.options.html.place // try to find where
        );
        this.displayDivName = this.options.html.displayClass;
        this.displayDivClass = `.${this.displayDivName}`;
        this.buttonsDivName = this.options.html.buttonsClass;
        this.buttonsDivClass = `.${this.buttonsDivName}`;

        // this is how user can enable/disable
        const addDisplay = () => {
            if (!this.place.select(this.displayDivClass).node()) {
                this.place.append("div").attr("class", this.displayDivName);
            }
        };
        const addButtons = () => {
            if (!this.place.select(this.buttonsDivClass).node()) {
                this.place.append("div").attr("class", this.buttonsDivName);
            }
        };
        if (this.options.html.buttonsAboveDisplay) {
            addButtons();
            addDisplay();
        } else {
            addDisplay();
            addButtons();
        }
        this.buttonsSelect = this.place.select(
            this.buttonsDivClass
        ) as typeof this.buttonsSelect;
        this.displaySelect = this.place.select(
            this.displayDivClass
        ) as typeof this.displaySelect;
        // now, create (if necessary) our buttons
        this.buttonsSelect
            .selectAll("button")
            .data(this.options.html.buttons)
            .join("button")
            .text((d) => d.text)
            .attr("class", (d) => `${d.classes.concat(d.addClasses).join(" ")}`)
            .on("click", (e, d: ButtonOptions) => {
                // figure out what to do, and do it! (at run time, maalesef)
                if (d.name === "generate") {
                    this.display();
                } else if (d.name === "clear") {
                    this.clear();
                } else if (d.name === "remove") {
                    this.remove();
                } else if (d.name === "save") {
                    this.save();
                }
            });
        // now, "remove chart" and "save image" are only
        // applicable when a chart is displayed, so hide those for now...
        this.buttonsSelect.selectAll(".onlychart").style("display", "none");
    }

    /**
     * generate a name for this
     *
     * @param process - name of this process within this module
     */
    _name(process: string) {
        return `(perfobars.js) ${this.clientName}:${process}`;
    }

    /**
     * invert _name()
     *
     * @param name - the _name'd string to invert
     *
     * @param leaveModule - do we leave in the module name, at least?
     */
    _unName(name: string, leaveModule: boolean) {
        return leaveModule
            ? name.replace(/^\(perfobars.js\) +/, "")
            : name.replace(/^\(perfobars.js\) [^:]*: */, "");
    }

    /**
     * PROCESS in MODULE (just a two-level naming hierarchy) has begun
     *
     * @param process - the (second-level) name of the process that is
     * starting.
     */
    begin(process: string) {
        const name = this._name(process),
            count = outstanding.get(name);
        window.performance.mark(`${name}: begin`);
        outstanding.set(name, count ? count + 1 : 1);
    }

    /**
     * PROCESS in MODULE has finished
     *
     * @param process - the (second-level) name of the process that is
     * finishing.  (should be the same name as was passed to `begin`.)
     */
    end(process: string) {
        const name = this._name(process),
            count = outstanding.get(name);

        window.performance.mark(`${name}: end`);
        // and, create a measure
        window.performance.measure(name, `${name}: begin`, `${name}: end`);

        if (!count) {
            // that's a problem
            alert("oops, no outstanding, perfobars.js");
        } else if (count === 1) {
            outstanding.delete(name);
        } else {
            outstanding.set(name, count - 1);
        }
    }

    /**
     * place a note
     * [("mark")](https://developer.mozilla.org/en-US/docs/Web/API/Performance/mark)
     * TEXT at this point; PROCESS can be blank.
     *
     * @param process - the (second-level) process to be marked
     * @param text - text to be added to the mark
     */
    mark(process: string, text: string) {
        window.performance.mark(`${this._name(process)}: mark ${text}`);
    }

    /**
     * return the list of
     * [marks](https://developer.mozilla.org/en-US/docs/Web/API/Performance/mark)
     * marked for this module
     *
     * @return the list of marks
     */
    ourMarks(): PerformanceEntry[] {
        return window.performance.getEntriesByType("mark").filter((d) => {
            return /^\(perfobars.js\)/.test(d.name);
        });
    }

    /**
     * return the list of
     * [measurements](https://developer.mozilla.org/en-US/docs/Web/API/Performance/measure)
     * marked for this module
     *
     * @return the list of marks
     */
    ourMeasures(): PerformanceEntry[] {
        return window.performance.getEntriesByType("measure").filter((d) => {
            return /^\(perfobars.js\)/.test(d.name);
        });
    }

    /**
     * clear all of our
     * [marks](https://developer.mozilla.org/en-US/docs/Web/API/Performance/mark)/[measures](https://developer.mozilla.org/en-US/docs/Web/API/Performance/measure)
     */
    clear() {
        this.ourMarks().forEach((d) => window.performance.clearMarks(d.name));
        this.ourMeasures().forEach((d) =>
            window.performance.clearMeasures(d.name)
        );
    }

    /**
     * @return a list of timings still outstanding (for error
     * checking, for example)
     */
    outstanding(): string[] {
        // XXX for some reason https://stackoverflow.com/a/66313878/1527747
        // this won't work:
        //  return [...outstanding].map((r) => this._unName(r[0], true));

        let arr: string[] = [];
        outstanding.forEach((val, key) => arr.push(key));
        return arr;
    }

    /*
     * build and display a report.  called when the "display" button is pressed.
     *
     * with thanks to [d3/observable](https://observablehq.com/@d3/horizontal-bar-chart)
     */
    display() {
        // get window geometry
        const // wWidth = window.innerWidth,
            wHeight = window.innerHeight;
        const margin = { top: 20, right: 20, bottom: 30, left: 75 };

        // svg, mouse events, etc.:
        // https://jarrettmeyer.com/2018/06/21/d3-capturing-mouse-events
        // https://chartio.com/resources/tutorials/how-to-show-data-on-mouseover-in-d3js/
        // https://www.smashingmagazine.com/2018/05/svg-interaction-pointer-events-property/

        if (!this.place) {
            // no html support defined in .html file
            throw new Error(
                `Perfobars: no element "${this.options.html.place}" in html file/DOM; perfobars.display() not available`
            );
            // NOTREACHED
        }

        const mouseOver = (() => {
            /**
             * @param e - the event
             * @param d - the "datum"
             */
            return (e: Event, d: PerformanceEntry) => {
                const pointer = d3.pointer(e);
                this.svg && this.svg.selectAll("rect").attr("opacity", 0.1);
                this.ttdiv &&
                    this.ttdiv
                        .attr(
                            "transform",
                            `translate(${pointer[0]},${pointer[1]})`
                        )
                        .attr("display", null)
                        .raise();
                this.ttdiv &&
                    this.ttdiv
                        .select("text")
                        .text(
                            `${this._unName(
                                d.name,
                                true
                            )}: duration ${formatNumber(
                                d.duration
                            )} [${formatNumber(d.startTime)}, ${formatNumber(
                                d.startTime + d.duration
                            )}]`
                        );
            };
        })();
        const mouseOut = (() => {
            return () => {
                // filter, to keep from switching back and forth when passing over tooltip text
                this.svg && this.svg.selectAll("rect").attr("opacity", 1);
                this.ttdiv && this.ttdiv.attr("display", "none");
            };
        })();

        if (this.outstanding().length) {
            console.log(
                "Perfobars: still some outstanding processes",
                this.outstanding()
            );
            alert(
                `Perfobars: still some outstanding processes: ${this.outstanding().join(
                    ", "
                )}`
            );
        }

        this.width = 0;
        if (this.displaySelect) {
            // so, node() returns "GElement", which extends
            // "BaseType", a union type that includes "Element".  we
            // want "HTMLElement" (to get .offsetWidth), which *also*
            // extends "Element".  so, i cast it.  it *seems* to work.
            // but, i'm unhappy with the procedure.  partly, i don't
            // know what, other than a (generic) "html element"
            // .node() could be returning.
            let tdn = this.displaySelect.node() as HTMLElement | null;
            if (tdn) {
                this.width = tdn.offsetWidth;
            }
        }
        this.height = this.width * this.options.svg.aspectRatio; // fairly random
        if (this.height >= wHeight / 2) {
            // too big
            this.height = wHeight / 2;
            this.width = this.height / this.options.svg.aspectRatio;
        }
        if (this.width < this.options.svg.minimumWidth) {
            this.width = this.options.svg.minimumWidth;
            this.height = this.width * this.options.svg.aspectRatio;
        }

        this.measures = this.ourMeasures();

        if (this.measures.length === 0) {
            return; // nothing here!
        }

        let maxNameLength = d3.max([
            0,
            ...this.measures.map((d) => d.name.length),
        ]);

        // https://developer.mozilla.org/en-US/docs/Web/CSS/Viewport_concepts
        // also: https://stackoverflow.com/a/44334570/1527747
        if (!this.svg) {
            this.svg = d3
                .create("svg")
                .attr("viewBox", `0 0 ${this.width} ${this.height}`)
                .attr("width", "100%")
                .attr("height", this.height);
            this.rects = this.svg
                .append("g")
                .attr("fill", "none")
                .selectAll("rect");
            this.gx = this.svg.append<SVGSVGElement>("g");

            this.gy = this.svg.append<SVGSVGElement>("g");

            // https://jarrettmeyer.com/2018/06/21/d3-capturing-mouse-events
            // https://chartio.com/resources/tutorials/how-to-show-data-on-mouseover-in-d3js/
            // https://www.smashingmagazine.com/2018/05/svg-interaction-pointer-events-property/
            // define a div for the tooltip.  we do this *after* the
            // above, so that our tooltip display *above* the bars, etc.
            // https://stackoverflow.com/a/17133220/1527747
            this.ttdiv = this.svg
                .append("g")
                .attr("y", -8)
                .attr("class", "mytooltip")
                .attr("pointer-events", "none") // keep mouse events from occuring here...
                .attr("display", "none");
            this.ttdiv.append("text").text("");
        }

        this.x = d3
            .scaleLinear()
            .domain([
                d3.min(this.measures, (d: PerformanceEntry) => d.startTime),
                d3.max(
                    this.measures,
                    (d: PerformanceEntry) => d.startTime + d.duration
                ),
            ])
            .range([
                margin.left + maxNameLength * 5,
                this.width - margin.right,
            ]);

        this.y = d3
            .scaleBand()
            .domain(this.measures.map((d) => d.name))
            .range([this.height - margin.bottom, margin.top]);

        this.xAxis = (
            g: d3.Selection<SVGGElement, undefined, null, undefined>
        ) =>
            g
                .attr(
                    "transform",
                    `translate(0,${this.height - margin.bottom})`
                )
                .call(
                    d3
                        .axisBottom(this.x)
                        .ticks(this.width / 80)
                        .tickSizeOuter(0)
                );

        this.yAxis = (
            g: d3.Selection<SVGGElement, undefined, null, undefined>
        ) =>
            g
                .attr(
                    "transform",
                    `translate(${margin.left + maxNameLength * 5},0)`
                )
                .call(
                    d3
                        .axisLeft(this.y)
                        .tickFormat((tick) => this._unName(tick, true))
                )
                .call((g) => g.select(".domain").remove());

        // set up axes with correct data
        this.gx.call(this.xAxis);
        this.gy.call(this.yAxis);

        this.rects = this.rects
            .data(this.measures)
            .join("rect")
            .attr("x", (d: PerformanceEntry) => this.x && this.x(d.startTime))
            .attr(
                "width",
                (d: PerformanceEntry) =>
                    this.x &&
                    this.x(d.startTime + d.duration) - this.x(d.startTime)
            )
            .attr("y", (d) => this.y && this.y(d.name))
            .attr("height", () => this.y && this.y.bandwidth())
            .attr("fill", "steelblue")
            .attr("stroke", "black")
            .on("mouseover", mouseOver)
            .on("mouseout", mouseOut);

        if (this.place && this.displayDivClass) {
            // XXX cast.
            let tpn = this.place
                .select(this.displayDivClass)
                .node() as HTMLDivElement;
            if (tpn) {
                tpn.append(this.svg.node());
            } // XXX
        }

        if (this.buttonsSelect) {
            // and, enable 'remove chart', 'save image'
            this.buttonsSelect?.selectAll(".onlychart").style("display", null);
        }
    }

    remove() {
        if (this.place && this.displaySelect) {
            this.displaySelect.selectAll("svg").remove();
        }
        if (this.buttonsSelect) {
            // now, commit suicide (taking "save image" with us)
            // and, enable 'remove chart', 'save image'
            this.buttonsSelect.selectAll(".onlychart").style("display", "none");
        }
    }

    save() {
        if (this.place && saveSvgAsPng && this.displaySelect) {
            // XXX cast
            saveSvgAsPng(
                this.displaySelect.selectAll("svg").node() as SVGElement,
                this.options.svg.imageName,
                { backgroundColor: this.options.svg.imageBackgroundColor }
            );
        }
    }

    /**
     * get (if OPTIONS is 'falsy') or set the default options used by
     * succeeding instances.
     *
     * @param options
     */
    static defaultOptions(options: Options) {
        if (!options) {
            return defaultOptions;
        } else {
            defaultOptions = options;
        }
    }

    /**
     * return the default options (in case one is interested)
     *
     * @returns the defaults "compiled" in to the code
     */
    installedDefaultOptions(): Options {
        return installedDefaultOptions;
    }
}

// utility functions

/**
 * getKeys -- get the keys of an object in a way typescript approves
 *
 * typescript doesn't like Object.keys(): https://stackoverflow.com/q/52856496/1527747
 *
 * so, solution is casting:
 * this routine: https://github.com/microsoft/TypeScript/issues/24243
 *
 * typescript casts in javascript code:
 * https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html
 */
function getKeys<T>(o: T): Array<keyof T> {
    // NB: when in *typescript* mode, prettier, or prettier.el uses
    // the "typescript" parser, rather than with the "babel" one.
    // and, that parser gets rid of the "unnecessary" parens around
    // the rvals; that breaks typescripts idea of a case!!
    // https://github.com/jscheid/prettier.el/issues/92 is somewhat
    // relevant
    let keys: Array<keyof T> = <Array<keyof T>>Object.keys(o);

    return keys;
}

/**
 * simple recursive merge of options from o2 into o1.  a *copy* of o1 is
 * returned; if NOEXTENSION, tags in O2 must already exist in O1 or an error is
 * signaled
 *
 * this could probably be generic (i.e., "merge()", rather than
 * "mergeOptions()"), but i'm not sure how to write the typescript types to
 * ensure it comes out looking like its input o1 (in terms of properties).
 *
 *: Options | HtmlOptions | ButtonOptions | SvgOptions>
 *
 */
function mergeOptions<T>(o1: T, o2: { [key in keyof T]: typeof o1[key] }): T {
    let n: T = _.cloneDeep(o1);

    if (typeof o1 !== "object" || typeof o2 !== "object") {
        // our simple merge only does types
        throw new Error(
            "Perfobars: mergeOptions() called with non-object argument"
        );
        // NOtREACHED
    }
    for (let p of getKeys(n)) {
        if (typeof o2[p] !== "object") {
            n[p] = o2[p];
        } else {
            n[p] = mergeOptions(n[p], o2[p]);
        }
    }
    return n;
}
