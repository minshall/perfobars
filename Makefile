ORGFILE=perfobars.org
PKGJSON=package.json
NPM=npm
NPX=npx
TSC=tsc

# where javascript, typescript sources live
JSSRCDIR = .
TSSRCDIR = .
# where typescript-generated declaration files live
TYPESDIR = .
# where jsdoc-generated documentation lives
JSDOCDIR = ./jsdoc
JSDOCCONFIG = ./jsdoc.json

# where we stage
STDIR = ./staging
STPUBLIC = ${STDIR}/public
STTANGLED = ${STDIR}/tangled

FAVICONSRC = perfobars-image.png
FAVICON = ${STPUBLIC}/favicon.ico

# https://www.typescriptlang.org/docs/handbook/declaration-files/dts-from-js.html
TSCOPTS = --lib es2020,dom --module es2020 --moduleResolution node
TYPESOPTS = --declaration --emitDeclarationOnly ${TSCOPTS}

TSFILES = perfobars.ts
# these are the output of the above (eligible to be cleaned)
TRANSPILED = $(subst .ts,.js,${TSFILES})
DTSFILES = ${TYPESDIR}/$(subst .ts,.d.ts,${TSFILES})

READMEORG = readme.org
READMEMD = readme.md

# *we* need staging/{tangled,public} (for our tangle target).
# *staging/makefile may have other directories to produce.
SKELDIRS= staging ${STTANGLED} ${STPUBLIC} \
				${TYPES} ${JSDOCDIR} ${SRCDIR} ${JSSRCDIR} ${TSSRCDIR}

${TYPESDIR}/%.d.ts: ${TSSRCDIR}/%.ts
	${NPX} ${TSC} $< ${TYPESOPTS} --outDir ${TYPESDIR}

${JSSRCDIR}/%.js: ${TSSRCDIR}/%.ts
	${NPX} ${TSC} ${TSCOPTS} $<

all: skeldirs tangle favicon lint ${TRANSPILED} staging dtsfiles

.PHONY : staging
staging: skeldirs tangle
	cd ${STDIR} && ${MAKE} --no-print-directory

skeldirs:
	@for dir in ${SKELDIRS}; do \
		if [ ! -e $${dir} ]; then mkdir -p $${dir}; fi \
	done

${TRANSPILED}: tsconfig.json

${FAVICON}: ${FAVICONSRC}
	convert $< -resize 64x64 $@

favicon: ${FAVICON}

tangle: ${ORGFILE}
	./dotangle.el ${ORGFILE}
	@touch tangle				# try to reduce the number of times we do this...

.PHONY: dtsfiles
dtsfiles: ${DTSFILES}

.PHONY: jsdoc
jsdoc:
	npx jsdoc -d ${JSDOCDIR} -c ${JSDOCCONFIG} ${TSFILES}

# use typescript to "lint" the program
.PHONY: lint
lint: ${TSFILES}
	npx eslint ${TSFILES}

.PHONY: typedoc
typedoc: ${TSFILES}
	npx typedoc --entryPoints perfobars.ts

# run us through pretty, and diff with ourselves
pretty-diff:
	@for file in ${TSFILES}; do \
		prettier $${file} | diff -u $${file} - \
	; done

npminstall: skeldirs tangle
	npm install
	cd ${STDIR} && ${MAKE} --no-print-directory npminstall

${READMEMD}: ${READMEORG}
	emacs $< --batch -f org-md-export-to-markdown --kill

readme: ${READMEMD}

# reveal ourselves to the world
npmpublishmajor: dtsfiles gitgoodtogo bumpmajor readme
	${NPM} publish --access public
npmpublishminor: dtsfiles gitgoodtogo bumpminor readme
	${NPM} publish --access public
npmpublishpatch: dtsfiles gitgoodtogo bumppatch readme
	${NPM} publish --access public

backup:							# make a copy of ${PKGJSON}
	cp -p ${PKGJSON} ${PKGJSON}.backup

# constant bits of the jq expression.  jq magic:
# https://stedolan.github.io/jq/tutorial/
JQTONUM='.|.version=([[(.version/".")|.[]|tonumber]|'
JQTOSTR='|.[]|tostring]|.[0]+"."+.[1]+"."+.[2])'
# modify version number in various ways
bumpmajor: backup
	jq ${JQTONUM}'[.[0]+1, 0, 0]'${JQTOSTR} < ${PKGJSON}.backup > ${PKGJSON}
bumpminor: backup
	jq ${JQTONUM}'[.[0], .[1]+1, 0]'${JQTOSTR} < ${PKGJSON}.backup > ${PKGJSON}
bumppatch: backup
	jq ${JQTONUM}'[.[0], .[1], .[2]+1]'${JQTOSTR} < ${PKGJSON}.backup > ${PKGJSON}

# check git
gitcleanp:
	@git status --short | awk '$$1 !~ /[?][?]/ { x++ } END {exit x}' || \
			(echo "ERROR: git working tree not clean" > /dev/stderr; exit 1)

# https://gist.github.com/justintv/168835#gistcomment-3012111
gitmasterp:
	@[[ `git symbolic-ref --short HEAD` = "master" ]] || \
			(echo "ERROR: git not on branch master" > /dev/stderr; exit 1)

gitgoodtogo: gitcleanp gitmasterp

# to test locally, "stuff" our local copy of perfobars in
# staging/node_modules/perfobars
.PHONY: stuff
stuff: ${DTSFILES} ${TSFILES} ${READMEMD} ${PKGJSON} | skeldirs
	rsync -a $^ ${STDIR}/node_modules/perfobars

clean:
	@rm -f tangle
	@rm -f ${DTSFILES}
	@rm -f ${TRANSPILED}
	@rm -f ${READMEMD}
	@cd ${STDIR} && echo also cleaning in ${STDIR} && make --no-print-directory clean

deepclean:
	@rm -rf ${JSDOCDIR}
	@cd ${STDIR} && make --no-print-directory deepclean

npmreallyclean: deepclean
	@rm -rf node_modules
	@cd ${STDIR} && make --no-print-directory npmreallyclean
