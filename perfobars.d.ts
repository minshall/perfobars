/**
 * @module Perfobars
 */
import * as d3 from "d3";
/**
 * Options - options that can be passed in to `new Perfobars`
 *
 * @param html - an instance of `HtmlOptions`
 *
 * @param svg - an instance of `SvgOptions`
 */
export declare type Options = {
    html: HtmlOptions;
    svg: SvgOptions;
};
/**
 * HtmlOptions describe where and how Perfobars is inserted in the
 * page.
 *
 * @param place - a `jquery` selector locating a place in the page for
 *      Perfobar's user interface elements
 *
 * @param displayClass - class for the `div` created to hold the UI
 * elements
 *
 * @param buttonsClass - class for the `div` created to hold the buttons
 *
 * @param buttonsAboveDisplay - if `true`, the buttons are above, as
 * opposed to below, the display
 *
 * @param buttons - an instance of `ButtonOptions`
 */
export declare type HtmlOptions = {
    place: string;
    displayClass: string;
    buttonsClass: string;
    buttonsAboveDisplay: boolean;
    buttons: ButtonOptions[];
};
/**
 * ButtonOptions allows the developer to configure how the
 * buttons Perfobars adds are inserted into the DOM.
 * @param addClasses - more classes added to this button
 * @param name - which button is being configured
 * @param classes - some of the classes we add to this button
 * @param text - text to appear on the button [`name`]
 */
export declare type ButtonOptions = {
    name: "generate" | "clear" | "remove" | "save";
    classes: string[];
    addClasses: string[];
    text: string;
};
/**
 * SvgOptions - details of the SVG holding the display
 *
 * @param aspectRatio - aspect ratio (height/width) [0.6]
 * @param imageName - name of saved .png ["perfobars-image.png"]
 * @param imageBackgroundColor - background color of saved .png ["grey"]
 * @param minimumWidth - minimum width on screen [300]
 */
export declare type SvgOptions = {
    aspectRatio: number;
    imageName: string;
    imageBackgroundColor: string;
    minimumWidth: number;
};
declare let defaultOptions: Options;
export default class Perf {
    clientName: string;
    options: Options;
    place: d3.Selection<HTMLDivElement, unknown, HTMLElement, unknown>;
    displayDivName: string;
    displayDivClass: string;
    buttonsDivName: string;
    buttonsDivClass: string;
    buttonsSelect: d3.Selection<HTMLDivElement, ButtonOptions, HTMLElement, unknown>;
    displaySelect: d3.Selection<d3.BaseType, SVGSVGElement, HTMLElement, unknown>;
    width: number;
    height: number;
    rects: d3.Selection<d3.BaseType, PerformanceEntry, SVGGElement, undefined>;
    xAxis: (g: d3.Selection<SVGGElement, undefined, null, undefined>) => d3.Selection<SVGGElement, undefined, null, undefined>;
    yAxis: (g: d3.Selection<SVGGElement, undefined, null, undefined>) => d3.Selection<SVGGElement, undefined, null, undefined>;
    svg: d3.Selection<SVGSVGElement, undefined, null, undefined>;
    ttdiv: d3.Selection<SVGGElement, undefined, null, undefined>;
    x: d3.ScaleLinear<number, number, never>;
    y: d3.ScaleBand<string>;
    measures: PerformanceEntry[];
    gx: d3.Selection<SVGSVGElement, undefined, null, undefined>;
    gy: d3.Selection<SVGSVGElement, undefined, null, undefined>;
    /**
     * CLIENTNAME is an arbitrary name for the user of *this* class
     * instance.  all classes share the same graphic, etc.
     *
     * by default, Perfobars attaches its elements after a DOM node with
     * 'id="perfobars-display"'.  so, the *default* value of ARGPLACE is
     * "#perfobars-display" (NB: notice the "#").  but, if *any* instance
     * is constructed with a non-null/undefined ARGPLACE, that is
     * where the graph (for all instances) will show up
     *
     * three buttons are instantiated:
     * - generate :: this generates a graph, using whatever data is in the database
     * - clear :: clears the database
     * - remove :: removes the graph from the DOM
     *
     * @param clientName - the name ("tag") of this client
     * @param options - options to override defaults
     */
    constructor(clientName: string, options?: {
        [key in keyof Options]: typeof defaultOptions[key];
    });
    /**
     * generate a name for this
     *
     * @param process - name of this process within this module
     */
    _name(process: string): string;
    /**
     * invert _name()
     *
     * @param name - the _name'd string to invert
     *
     * @param leaveModule - do we leave in the module name, at least?
     */
    _unName(name: string, leaveModule: boolean): string;
    /**
     * PROCESS in MODULE (just a two-level naming hierarchy) has begun
     *
     * @param process - the (second-level) name of the process that is
     * starting.
     */
    begin(process: string): void;
    /**
     * PROCESS in MODULE has finished
     *
     * @param process - the (second-level) name of the process that is
     * finishing.  (should be the same name as was passed to `begin`.)
     */
    end(process: string): void;
    /**
     * place a note
     * [("mark")](https://developer.mozilla.org/en-US/docs/Web/API/Performance/mark)
     * TEXT at this point; PROCESS can be blank.
     *
     * @param process - the (second-level) process to be marked
     * @param text - text to be added to the mark
     */
    mark(process: string, text: string): void;
    /**
     * return the list of
     * [marks](https://developer.mozilla.org/en-US/docs/Web/API/Performance/mark)
     * marked for this module
     *
     * @return the list of marks
     */
    ourMarks(): PerformanceEntry[];
    /**
     * return the list of
     * [measurements](https://developer.mozilla.org/en-US/docs/Web/API/Performance/measure)
     * marked for this module
     *
     * @return the list of marks
     */
    ourMeasures(): PerformanceEntry[];
    /**
     * clear all of our
     * [marks](https://developer.mozilla.org/en-US/docs/Web/API/Performance/mark)/[measures](https://developer.mozilla.org/en-US/docs/Web/API/Performance/measure)
     */
    clear(): void;
    /**
     * @return a list of timings still outstanding (for error
     * checking, for example)
     */
    outstanding(): string[];
    display(): void;
    remove(): void;
    save(): void;
    /**
     * get (if OPTIONS is 'falsy') or set the default options used by
     * succeeding instances.
     *
     * @param options
     */
    static defaultOptions(options: Options): Options;
    /**
     * return the default options (in case one is interested)
     *
     * @returns the defaults "compiled" in to the code
     */
    installedDefaultOptions(): Options;
}
export {};
